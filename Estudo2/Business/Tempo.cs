﻿    using System;
using System.Windows.Forms;

namespace Estudo2.Business
{
    class Tempo
    {
        Timer _Temporizador = new Timer();
        int _Contador = 60;
        bool _IsIniciado;
        bool _IsPausado;
        bool _IsParado;
        string _Log = "";
        
        public int Contador { get => _Contador; }
        public bool IsIniciado { get => _IsIniciado; }
        public bool IsPausado { get => _IsPausado; }
        public bool IsParado { get => _IsParado; }
        public string Log { get => _Log; }

        public void Iniciar()
        {            
            _Temporizador.Enabled = true;
            _Temporizador.Tick += new EventHandler(Temporizador_Tick);
            _Temporizador.Interval = 1000;
            _Temporizador.Start();
            _IsIniciado = true;
        }

        public void Pausar()
        {
            if (_IsIniciado)
            {
                _Temporizador.Stop();
                _Log = _Contador.ToString();
                _IsPausado = true;
            }
        }

        public void Parar()
        {
            if(_IsIniciado || _IsPausado)
            {
                _Temporizador.Dispose();
                _Temporizador.Enabled = false;
                _Log = _Contador.ToString();
                _IsParado = true;
            }
        }

        private void Temporizador_Tick(object sender, EventArgs e)
        {
            _Contador--;

            if(_Contador == 0)
            {
                _Temporizador.Stop();
                _Temporizador.Dispose();
                _Log = _Contador.ToString();
            }
            //label.Text = Contador.ToString();
        }
    }
}
